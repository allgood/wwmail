var webpack = require('webpack');

module.exports = {
    mode: "development",
    devtool: "inline-source-map",
    context: __dirname,
    entry: {
       wwmailui: "./js/wwmail-ui.js"
    },
    resolve: {
       alias: {
          'handlebars' : 'handlebars/dist/handlebars.js'
       }
    },
    node: {
       fs: "empty"
    },
    output: {
        path: __dirname + "/build/",
        filename: "[name]-webpack.js"
    },
    module: {
        rules: [
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                use: [
                  {
                    loader: 'url-loader',
                    options: {
                      limit: 100000
                    }
                  }
                ]
            },
            {
                test: /\.less$/,
                use: [
                  {
                    loader: "style-loader"
                  }
                ]
            }

        ]
    },
    plugins: [
    new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery"
    })
    ]
//    plugins: [
//        new webpack.optimize.UglifyJsPlugin({
//            compress: {
//                warnings: false
//            }
//        })
//    ]
}
