<?php

function base64url_encode($data) {
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode($data) {
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

function file_read_without_comments($filepath) {
  $string='';
  $file=fopen($filepath,'r');
  while (($line = fgets($file)) !== false) {
    if (trim($line)[0] != "#") {
      $string.=trim($line)."\n";
    }
  }
  fclose($file);
  return $string;
}

$htmlhead=
"<html>
 <head>
   <title>Liberação envio de Mensagem</title>
   <meta name='viewport' content='width=device-width, user-scalable=no'>
   <link rel='stylesheet' type='text/css' href='wwmail.css'>
 </head>
 <body>
  <div id='main'>";

$htmlform="
<form>

</form>
";

$htmlfoot="
  </div>
 </body>
</html>";

$htmlrecaptchabutton='
<p>Para confirmar o envio da mensagem, clique no botão abaixo:</p>
<form id="unfreezeForm" action="unfreeze.php" method="post">
<script>function submitCallback(v) { document.getElementById("unfreezeForm").submit(); };</script>
<input type="hidden" name="messageID" value="%s"><input type="hidden" name="ts" value="%s"><input type="hidden" name="hash" value="%s">
<button
class="g-recaptcha"
data-sitekey="%s"
data-callback="submitCallback">Confirmar Envio</button>
</form>
';


echo $htmlhead;
$config = parse_ini_string(file_read_without_comments("/etc/default/wwmail"));

if ($config["WW_RECAPTCHA_SITE"] && $config["WW_RECAPTCHA_SECRET"]) {
    echo "<script src='https://www.google.com/recaptcha/api.js'></script>";
    echo "<script src='wwmail.js'></script>";
}

echo "<h2>Liberação de envio</h2>";


$ts = reset(unpack("N",base64url_decode($_REQUEST["ts"])));
$hash = trim(base64url_encode(md5($_REQUEST["messageID"].$_REQUEST["ts"].$config["WW_ISP_PASS"],TRUE)),"=");

if ($hash==$_REQUEST["hash"]) {

   if ($config["WW_RECAPTCHA_SITE"] && $config["WW_RECAPTCHA_SECRET"] && !$_REQUEST["g-recaptcha-response"]) {
      echo "<p>Por favor, analise os dados da mensagem abaixo:</p>";
   }

   echo "<pre>";
   exec("/usr/bin/sudo /usr/bin/mailq ".$_REQUEST["messageID"],$queuemsg);
   echo htmlspecialchars(implode("\n",$queuemsg));
   echo "</pre>";

   if ($config["WW_RECAPTCHA_SITE"] && $config["WW_RECAPTCHA_SECRET"] && !$_REQUEST["g-recaptcha-response"]) {
      printf($htmlrecaptchabutton,$_REQUEST["messageID"], $_REQUEST["ts"] , $_REQUEST["hash"], $config["WW_RECAPTCHA_SITE"]);
   } else {
      $valid = TRUE;
      if ($config["WW_RECAPTCHA_SITE"] && $config["WW_RECAPTCHA_SECRET"]) {
         $valid = FALSE;
         $validatecaptcha = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$config["WW_RECAPTCHA_SECRET"]."&response=".$_REQUEST["g-recaptcha-response"]."&remoteip=".$_SERVER['REMOTE_ADDR']),TRUE);
         $valid = $validatecaptcha["success"];
      }
      if ($valid) {
         echo "<p>Autorizando o envio:</p>";
         exec("/usr/bin/sudo /usr/sbin/exim4 -Mt ".$_REQUEST["messageID"],$thawmsg);
         echo "<pre>".htmlspecialchars(implode("\n",$thawmsg))."</pre>";
      } else {
         echo "\n<b>Verificação falhou</b>\n";
      }
   }
} else {
   echo("Hash inválido");
}

echo $htmlfoot;
