<?php 
header('Content-Type: text/xml');

function base64url_encode($data) {
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode($data) {
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

function file_read_without_comments($filepath) {
  $string='';
  $file=fopen($filepath,'r');
  while (($line = fgets($file)) !== false) {
    if (trim($line)[0] != "#") {
      $string.=trim($line)."\n";
    }
  }
  fclose($file);
  return $string;
}

$config = parse_ini_string(file_read_without_comments("/etc/default/wwmail"));

$xml="<?xml version='1.0' encoding='UTF-8'?>

<clientConfig version='1.1'>
  <emailProvider id='${config['WW_MAIL_DOMAIN']}'>
    <domain>${config['WW_MAIL_DOMAIN']}</domain>
    <displayName>${config['WW_MAIL_NAME']}</displayName>
    <displayShortName>${config['WW_MAIL_NAME']}</displayShortName>
    <incomingServer type='imap'>
      <hostname>${config['WW_SSL_MAIN']}</hostname>
      <port>143</port>
      <socketType>STARTTLS</socketType>
      <authentication>password-cleartext</authentication>
      <username>%EMAILADDRESS%</username>
    </incomingServer>
    <incomingServer type='imap'>
      <hostname>${config['WW_SSL_MAIN']}</hostname>
      <port>993</port>
      <socketType>SSL</socketType>
      <authentication>password-cleartext</authentication>
      <username>%EMAILADDRESS%</username>
    </incomingServer>
    <outgoingServer type='smtp'>
      <hostname>${config['WW_SSL_MAIN']}</hostname>
      <port>587</port>
      <socketType>STARTTLS</socketType>
      <authentication>password-cleartext</authentication>
      <username>%EMAILADDRESS%</username>
    </outgoingServer>
    <documentation url='https://${config['WW_SSL_MAIN']}'>
      <descr lang='en'>${config['WW_MAIL_NAME']} Main Site</descr>
      <descr lang='pt'>${config['WW_MAIL_NAME']} Site Principal</descr>
    </documentation>
  </emailProvider>
</clientConfig>
";

echo $xml;
