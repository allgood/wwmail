
window.make_base_auth = function(user, password) {
  var tok = user + ':' + password;
  var hash = btoa(tok);
  return "Basic " + hash;
}

window.activateBox = function (name) {
  $(".contentbox").hide();
  $(name).show();
}

window.message = function(text) {
  $('#messagetext').html(text);
  activateBox('#message');
}

window.messageOk = function(text) {
  message(text);
}

window.messageError = function(text) {
  message(text);
}

$(document).ready( function() {

  path = window.location.pathname.split( '/' );
  page = path[path.length - 1];

  switch (page) {
    case 'otp':
      otp = require('./otp');
      otp();
      break;
    default:
      activateBox('#invalid');
  }

  $("button#reload").click( function() {
    location.reload();
  });

});
