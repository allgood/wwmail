function otp() {

  QR=require('qrcode-generator-es6').default;

  function otpStart(loginfield, pwdfield, otpcode) {
    otpcode = otpcode || 0;
    $.ajax
      ({
        type: "GET",
        url: "/wwmail/api/otp/start/"+loginfield+(otpcode?("/"+otpcode):""),
        beforeSend: function (xhr){
          xhr.setRequestHeader('Authorization', make_base_auth(loginfield, pwdfield));
        },
        success: function (data){
          myel = document.getElementById("qrcode");

          const qrcode = new QR(0, 'H');
          qrcode.addData(data.uri);
          qrcode.make();
          myel.innerHTML = qrcode.createSvgTag({});

          $('#qrcodelink').attr('href',data.uri);

          $("button#setupotp").click( function() {
            otpcode = $("#otpcode").val();
            otpConfirm(loginfield,pwdfield,otpcode);
          })

          activateBox('#otpqr');
        },
        error: function () {
          if (otpcode > 0) {
            messageError("Código incorreto, tente de novo.");
          } else {
            messageError("Erro ao gerar código.");
          }
        }
    });
  }

  function otpConfirm(loginfield, pwdfield, otpcode) {
    $.ajax
      ({
        type: "GET",
        url: "/wwmail/api/otp/confirm/"+loginfield+"/"+otpcode,
        beforeSend: function (xhr){
          xhr.setRequestHeader('Authorization', make_base_auth(loginfield, pwdfield));
        },
        success: function (data){
          messageOk("Segundo fator configurado com sucesso!");
        },
        error: function () {
          messageError("Código incorreto, tente novamente.");
        }
    });
  }

  $("form#login").submit( function(form) {

    form.preventDefault();

    loginfield = $("#loginfield").val();
    pwdfield = $("#pwdfield").val();

    $.ajax
      ({
        type: "GET",
        url: "/wwmail/api/user/"+loginfield,
        beforeSend: function (xhr){
           xhr.setRequestHeader('Authorization', make_base_auth(loginfield, pwdfield));
        },
        success: function (data){
          if (data[0].totpactive) {
            $('button#b-reset').click( function() {
              otpresetcode = $("#otpresetcode").val();
              otpStart(loginfield, pwdfield, otpresetcode);
            })
            activateBox("#otpreset");
          } else {
            otpStart(loginfield, pwdfield);
          }
        },
        error: function () {
          messageError("Senha incorreta");
        }
    });

  });

  $(".title").each( function (e) { $(this).html("Configuração de Segundo Fator") });

  activateBox('#loginform');
};

module.exports = otp;
