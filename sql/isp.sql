-- DUMP command: mysqldump -u root -d -R -N --default-character-set=utf8 -p --databases isp
-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: isp
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB-0+deb9u1
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `isp`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `isp` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `isp`;

--
-- Table structure for table `aliases`
--

DROP TABLE IF EXISTS `aliases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aliases` (
  `address` varchar(255) NOT NULL DEFAULT '',
  `goto` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dkim`
--

DROP TABLE IF EXISTS `dkim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dkim` (
  `domain` varchar(128) NOT NULL,
  `selector` varchar(128) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `privkey` text NOT NULL,
  UNIQUE KEY `domain` (`domain`,`selector`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `domainalias`
--

DROP TABLE IF EXISTS `domainalias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domainalias` (
  `domain` varchar(255) NOT NULL,
  `goto` varchar(255) NOT NULL,
  PRIMARY KEY (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `domains`
--

DROP TABLE IF EXISTS `domains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(128) NOT NULL,
  `storage` int(11) NOT NULL DEFAULT '5000',
  `accounts` int(11) NOT NULL DEFAULT '10',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain` (`domain`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `insert_domain` BEFORE INSERT ON `domains` FOR EACH ROW BEGIN
   -- Check domain quota
   IF NEW.storage > 0 AND get_used_quota(NEW.domain) > NEW.storage
   THEN
      SIGNAL SQLSTATE '45000'
        	SET MESSAGE_TEXT = "Domain quota exceeded";
   END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `update_domain` BEFORE UPDATE ON `domains` FOR EACH ROW BEGIN
   -- Check domain quota
   IF NEW.storage > 0 AND get_used_quota(NEW.domain) > NEW.storage
   THEN
      SIGNAL SQLSTATE '45000'
        	SET MESSAGE_TEXT = "Domain quota exceeded";
   END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `delete_domain` BEFORE DELETE ON `domains` FOR EACH ROW BEGIN
   DECLARE usercount INT(11);

   SELECT COUNT(*) FROM users WHERE gen_domain = OLD.domain INTO usercount;

-- Check domain accounts
   IF usercount>0
   THEN
      SIGNAL SQLSTATE '45000'
        	SET MESSAGE_TEXT = "Domain have users";
   END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `fromblacklist`
--

DROP TABLE IF EXISTS `fromblacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fromblacklist` (
  `address` varchar(100) NOT NULL DEFAULT '',
  UNIQUE KEY `address` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fromwhitelist`
--

DROP TABLE IF EXISTS `fromwhitelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fromwhitelist` (
  `address` varchar(100) NOT NULL DEFAULT '',
  UNIQUE KEY `address` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mailip`
--

DROP TABLE IF EXISTS `mailip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailip` (
  `address` varchar(15) NOT NULL,
  `hostname` varchar(128) NOT NULL,
  `blacklisted` int(4) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `address` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `monitoring`
--

DROP TABLE IF EXISTS `monitoring`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monitoring` (
  `domain` varchar(128) NOT NULL,
  `address` varchar(128) NOT NULL,
  PRIMARY KEY (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(128) NOT NULL,
  `target` varchar(128) NOT NULL,
  `role` enum('admin','support') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `target` (`target`),
  CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`address`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
ALTER DATABASE `isp` CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `validate_target_insert` BEFORE INSERT ON `roles` FOR EACH ROW BEGIN
	IF TRIM(NEW.target)<>'' AND (SELECT COUNT(domain) FROM domains WHERE domain=NEW.target) + (SELECT COUNT(address) FROM users WHERE users.address=NEW.target) = 0
    THEN
    	SIGNAL SQLSTATE '45000'
        	SET MESSAGE_TEXT = "foreign target inexistant";
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `isp` CHARACTER SET utf8 COLLATE utf8_general_ci ;
ALTER DATABASE `isp` CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `validate_target_update` BEFORE UPDATE ON `roles` FOR EACH ROW BEGIN
	IF TRIM(NEW.target)<>'' AND (SELECT COUNT(domain) FROM domains WHERE domain=NEW.target) + (SELECT COUNT(address) FROM users WHERE users.address=NEW.target) = 0
    THEN
    	SIGNAL SQLSTATE '45000'
        	SET MESSAGE_TEXT = "foreign target inexistant";
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `isp` CHARACTER SET utf8 COLLATE utf8_general_ci ;

--
-- Table structure for table `rrd_ratelimit`
--

DROP TABLE IF EXISTS `rrd_ratelimit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rrd_ratelimit` (
  `address` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `counter` int(11) NOT NULL,
  UNIQUE KEY `addresstime` (`address`,`time`),
  KEY `address` (`address`,`time`,`counter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rrd_send`
--

DROP TABLE IF EXISTS `rrd_send`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rrd_send` (
  `address` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `counter` int(11) NOT NULL,
  UNIQUE KEY `addresstime` (`address`,`time`),
  KEY `address` (`address`,`time`,`counter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sieve`
--

DROP TABLE IF EXISTS `sieve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sieve` (
  `address` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `script` mediumtext NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  UNIQUE KEY `addressname` (`address`,`name`),
  KEY `address` (`address`),
  KEY `name` (`name`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `systemaliases`
--

DROP TABLE IF EXISTS `systemaliases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemaliases` (
  `address` varchar(255) NOT NULL DEFAULT '',
  `goto` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `address` varchar(128) NOT NULL DEFAULT '',
  `crypt` varchar(128) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL DEFAULT '',
  `maildir` varchar(255) NOT NULL,
  `quota` int(11) NOT NULL DEFAULT '45',
  `spamreject` int(11) NOT NULL DEFAULT '6',
  `lastread` datetime NOT NULL,
  `totpsecret` varchar(128) NOT NULL,
  `totpactive` int(11) NOT NULL DEFAULT '0',
  `mail` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `gen_domain` varchar(128) AS (REGEXP_SUBSTR(address,'[^@]+$')) PERSISTENT,
  UNIQUE KEY `maildir` (`maildir`),
  UNIQUE KEY `address` (`address`),
  KEY `lastread` (`lastread`),
  KEY `gen_domain` (`gen_domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `insert_user` BEFORE INSERT ON `users` FOR EACH ROW BEGIN
   DECLARE u_domain VARCHAR(128);
   DECLARE d_quota INT(11);

   -- Default value for maildir
   IF (NEW.maildir IS NULL OR NEW.maildir = '') THEN
      SET NEW.maildir = REGEXP_REPLACE(NEW.address , '([^@]+)@(.+)$', '/var/spool/maildir/\\2/\\1/');
   END IF;

   -- Check domain quota
   SET u_domain = REGEXP_REPLACE(NEW.address , '[^@]+@(.+)$', '\\1');
   SELECT storage FROM domains WHERE domains.domain = u_domain INTO d_quota;

   IF d_quota > 0 AND get_used_quota(u_domain) + NEW.quota > d_quota
   THEN
      SIGNAL SQLSTATE '45000'
        	SET MESSAGE_TEXT = "Domain quota exceeded";
   END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `update_user` BEFORE UPDATE ON `users` FOR EACH ROW BEGIN
   DECLARE u_domain VARCHAR(128);
   DECLARE d_quota INT(11);

   -- Check domain quota
   SET u_domain = REGEXP_REPLACE(NEW.address , '[^@]+@(.+)$', '\\1');
   SELECT storage FROM domains WHERE domains.domain = u_domain INTO d_quota;

   IF d_quota > 0 AND get_used_quota(u_domain) + NEW.quota - OLD.quota > d_quota
   THEN
      SIGNAL SQLSTATE '45000'
        	SET MESSAGE_TEXT = "Domain quota exceeded";
   END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `volume_senders`
--

DROP TABLE IF EXISTS `volume_senders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volume_senders` (
  `address` varchar(128) NOT NULL,
  UNIQUE KEY `address` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `whitelist`
--

DROP TABLE IF EXISTS `whitelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `whitelist` (
  `address` varchar(128) NOT NULL,
  PRIMARY KEY (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'isp'
--
/*!50003 DROP FUNCTION IF EXISTS `get_used_quota` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_used_quota`(`p_domain` VARCHAR(128) CHARSET utf8) RETURNS int(11)
    NO SQL
    DETERMINISTIC
BEGIN
    DECLARE sumquota INT(11);
    SELECT SUM(quota) FROM users WHERE users.gen_domain = p_domain INTO sumquota;
    RETURN sumquota;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `unalias` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `unalias`(`p_address` VARCHAR(255) CHARSET utf8) RETURNS varchar(255) CHARSET utf8
BEGIN
	DECLARE a_address VARCHAR(255);
    DECLARE p_localpart VARCHAR(255);
    DECLARE p_domainpart VARCHAR(255);
    DECLARE p_domainalias VARCHAR(255);


    SET a_address = __unalias(p_address);

	IF NOT ISNULL(a_address) THEN
		RETURN a_address;
	END IF;


	SET p_domainpart = SUBSTRING(p_address, LOCATE('@', p_address) + 1);
    SET p_localpart = SUBSTRING(p_address, 1, LOCATE('@', p_address) - 1);


    SELECT goto FROM domainalias WHERE domain = p_domainpart
		INTO p_domainalias;

	IF NOT ISNULL(p_domainalias) THEN
		SET a_address = __unalias(CONCAT(p_localpart,'@',p_domainalias));
	END IF;

	IF NOT ISNULL(a_address) THEN
		RETURN a_address;
	END IF;


    SELECT u.address FROM systemaliases a,users u
       WHERE a.address = p_localpart AND a.goto = u.address
       INTO a_address;

RETURN a_address;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `__unalias` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `__unalias`(`p_address` VARCHAR(255) CHARSET utf8) RETURNS varchar(255) CHARSET utf8
BEGIN
	DECLARE a_address VARCHAR(255);
    DECLARE p_localpart VARCHAR(255);
    DECLARE p_domainpart VARCHAR(255);
    DECLARE p_domainalias VARCHAR(255);


    SELECT u.address FROM aliases a,users u
       WHERE a.address = p_address AND a.goto = u.address
       INTO a_address;

	IF NOT ISNULL(a_address) THEN
		RETURN a_address;
	END IF;


    SELECT u.address FROM users u
       WHERE u.address = p_address
       INTO a_address;

RETURN a_address;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `record_rrd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `record_rrd`(IN `vaddress` VARCHAR(255) CHARSET utf8, IN `vtable` VARCHAR(30) CHARSET utf8)
BEGIN
  DECLARE timeslot DATETIME;
  DECLARE ruid INT;

  SET timeslot = from_unixtime(300 * floor(unix_timestamp(now())/300));

  SET @delquery = CONCAT ( "DELETE FROM " , vtable , " WHERE `time` < DATE_SUB( NOW() , INTERVAL 24 HOUR ); " ) ;
  SET @updquery = CONCAT ( "INSERT INTO " , vtable , " ( address, `time` , counter ) VALUES ( '" , vaddress , "' , '" , timeslot , "' , 1 ) ON DUPLICATE KEY UPDATE counter=counter+1;" ) ;
  SET @rstquery = CONCAT ( "SELECT counter FROM " , vtable , " WHERE `time`='" , timeslot , "' AND address = '" , vaddress , "' ; " ) ;

  IF vtable = "rrd_ratelimit" THEN
     SELECT user_id INTO ruid FROM roundcube.users WHERE username = vaddress;
     DELETE FROM roundcube.contacts WHERE user_id = ruid AND changed >= DATE_SUB( NOW() , INTERVAL 10 MINUTE );
  END IF;


  PREPARE del_query FROM @delquery;
  EXECUTE del_query;

  PREPARE upd_query FROM @updquery;
  EXECUTE upd_query;

  PREPARE rst_query FROM @rstquery;
  EXECUTE rst_query;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `record_send` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `record_send`(IN `vaddress` VARCHAR(255) CHARSET utf8)
BEGIN
  DECLARE timeslot DATETIME;

  SET timeslot = from_unixtime(300 * floor(unix_timestamp(now())/300));

  DELETE FROM rrd_send WHERE `time` < DATE_SUB( NOW() , INTERVAL 24 HOUR );

  INSERT INTO rrd_send ( address, `time` , counter ) VALUES ( vaddress , timeslot , 1 ) ON DUPLICATE KEY UPDATE counter=counter+1;

  SELECT counter FROM rrd_send WHERE `time`=timeslot AND address = vaddress;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-17 12:13:36
