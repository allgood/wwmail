![ww-mail.png](https://bitbucket.org/repo/XE7r6y/images/1705615069-ww-mail.png)

# README #

This repository keeps configuration files and helper scripts necessary
to make the installation of a mail server using:

* Debian
* MySQL
* Exim
* Dovecot
* Roundcube
* PySieved
* SpamAssassin
* ClamAV
* LetsEncrypt

Some comments and messages are in Portuguese, they will be changed
eventually

### Features

* Basic setup of SPF, SRS, DNSBL, SpamAssassin, ClamAV, greylist, Sieve and WebMail
* SSL support for SMTP/Submission/POP/IMAP/Web
* DKIM support for both receiving and sending
* BATV support to avoid fake bounces
* Automatic SSL certificate creation and update through [LetEncrypt.org](http://letsencrypt.org)
* Automatic filling of contact books when sending messages
* Automatic learning as **HAM** when receiving message from known contacts
* Automatic learning of **SPAM** and **HAM** when moving to or from Junk folder
* Try to unsubscribe from mailing lists when moving to Junk folder
* User defined Spam threshold
* Granular or domain wide user monitoring through message copies to manager

### In development

* Freeze some messages with suspicious patterns and ask for confirmation

### Planned features

* SNI Support (multiple server names) on all services (Webmail, Dovecot and Exim)
* SRS code rewriting (possible using the one from Exim wiki)
* Two factor authentication
* Device/Application specific passwords (multiple and very long passwords)

### Contributions

Contributions are accepted through pull requests if they are clean

### Licence

The licence of all files here are GPL v2

**WW-Mail** is the name of the mail server product from **WW Teleinformática Ltda**
and should not be used on comercial products derivated of this code.
