#!/bin/bash

MYPID=$$

[ -d /var/run/sa-learn-queue ] || mkdir /var/run/sa-learn-queue

[ -f /var/run/sa-learn-queue/sa-learn-queue.pid ] \
   && SAQ_PID=$(cat /var/run/sa-learn-queue/sa-learn-queue.pid) \
   && [ -d /proc/${SAQ_PID} ] && grep -q sa-learn-queue.sh /proc/${SAQ_PID}/cmdline \
   && exit 0

echo ${MYPID} > /var/run/sa-learn-queue/sa-learn-queue.pid

## exit if pid changed after 2 seconds
sleep 2
SAQ_PID=$(cat /var/run/sa-learn-queue/sa-learn-queue.pid)
[ ${MYPID} -eq  ${SAQ_PID} ] || exit 0

/usr/bin/logger -p mail.notice -t sa-learn-queue\[${MYPID}\] "started daemon"
  
tail -f /var/spool/maildir/salearn.pipe | while read SA_ACTION SA_USER SA_FILENAME ;  do 
  /usr/bin/logger -p mail.notice -t sa-learn-queue\[${MYPID}\] -- "learning ${SA_ACTION} -u ${SA_USER} \"${SA_FILENAME}\""
    
  /usr/bin/sa-learn ${SA_ACTION} -u ${SA_USER} "${SA_FILENAME}" &> /dev/null
  if [ "$SA_ACTION" == "--spam" ] ; then
     cat "${SA_FILENAME}" | /usr/local/bin/unsubscriber.py
  fi
  rm -f "${SA_FILENAME}"
done

/usr/bin/logger -p mail.notice -t sa-learn-queue\[${MYPID}\] "stopping daemon (tail terminated)"
