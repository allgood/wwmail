#!/bin/bash

cat /etc/default/wwmail | sed -e 's/: /=/g' > /tmp/wwmail.$$.tmp
source /tmp/wwmail.$$.tmp
rm /tmp/wwmail.$$.tmp

export LOCAL_PART=$1
export DOMAIN=$2
export MONITOR=$3
export MAILFILE=$4

### para courier
#BASEFOLDER="INBOX."
#SUBSCRIPTIONFILE=courierimapsubscribed

### para dovecot
BASEFOLDER=""
SUBSCRIPTIONFILE=subscriptions


MESSAGEID=`cat $MAILFILE | /bin/sed -e '/^$/ q' | /bin/grep -i '^message-id:' | /bin/sed -e 's/[Mm]essage-[Ii][Dd]: <\([^>]*\).*/\1/g'`

## nao monitora o monitor

if [ $MONITOR == $LOCAL_PART@$DOMAIN ] ; then
  exit 0
fi

MONITORDIR=$(echo "select maildir from users where address='$MONITOR';" | mysql -h ${WW_ISP_HOST} -p${WW_ISP_PASS} -r -N -u${WW_ISP_USER} ${WW_ISP_DB})
if ! [ $MONITORDIR ] ; then
  exit 0
fi

if ! [ -d $MONITORDIR ] ; then
  exit 0
fi

if ! [ -d $MONITORDIR/.monitor ] ; then
  /usr/bin/maildirmake.maildrop -f monitor $MONITORDIR &> /dev/null
fi

if ! [ -d $MONITORDIR/.monitor.$LOCAL_PART ] ; then
  /usr/bin/maildirmake.maildrop -f monitor.$LOCAL_PART $MONITORDIR &> /tmp/monitor.log
  echo ${BASEFOLDER}monitor.$LOCAL_PART >> ${MONITORDIR}/${SUBSCRIPTIONFILE}
fi

if [ -f $MONITORDIR/.monitor.$LOCAL_PART/mid.txt ] ; then
  if grep $MESSAGEID $MONITORDIR/.monitor.$LOCAL_PART/mid.txt; then
     exit 0
  fi
fi

echo $MESSAGEID >> $MONITORDIR/.monitor.$LOCAL_PART/mid.txt
cat $MAILFILE | /usr/sbin/deliverquota.maildrop $MONITORDIR/.monitor.$LOCAL_PART &> /dev/null
