#!/bin/bash

[ -d /var/spool/maildir/salearn-queue ] || mkdir /var/spool/maildir/salearn-queue

TEMPFILE=/var/spool/maildir/salearn-queue/salearn-msg-$$.$RANDOM.msg
cat - > ${TEMPFILE}

## DEBUG
## echo /usr/bin/sa-learn $* /tmp/sendmail-msg-$$.txt

### check if queue is being read
( [ -f /var/run/sa-learn-queue/sa-learn-queue.pid ] \
   && SAQ_PID=$(cat /var/run/sa-learn-queue/sa-learn-queue.pid) \
   && [ -d /proc/${SAQ_PID} ] && grep -q sa-learn-queue.sh /proc/${SAQ_PID}/cmdline
) || /usr/bin/sudo -u root /usr/local/bin/sa-learn-runqueue.sh


while [ $1 ] ; do
    case $1 in
    --spam)
         SACOMMAND=--spam
         ;;
    --ham)
         SACOMMAND=--ham
         ;;
    -u)
         shift
         SAUSER=$1
         ;;
    esac
    shift
done


[ -e /var/spool/maildir/salearn.pipe ] || ( /usr/bin/mkfifo /var/spool/maildir/salearn.pipe && /bin/chmod ugo+r /var/spool/maildir/salearn.pipe )

[ "${SACOMMAND}" ] && [ "${SAUSER}" ] && [ "${TEMPFILE}" ] && echo "${SACOMMAND} ${SAUSER} ${TEMPFILE}" > /var/spool/maildir/salearn.pipe
