#!/bin/bash

cat /etc/default/wwmail | sed -e 's/: /=/g' > /tmp/wwmail.$$.tmp
source /tmp/wwmail.$$.tmp
rm /tmp/wwmail.$$.tmp

maildomain=
MAILDIR=/var/spool/maildir

## parametros
## -domain  altera dominio do email
## -PREFIX  altera PREFIXo do id
## -SUFIX   altera SUFIXo do id
## -maildir altera diretorio para as mensagens

DONE=

while ! [ $DONE ] ; do
   if [ "$1" == "-domain" ] ; then
      maildomain=$2
      shift
      shift
   elif [ "$1" == "-maildir" ] ; then
      MAILDIR=$2
      shift
      shift
   else
      DONE=1
   fi
done

if ! [ ${maildomain} ] ; then
  maildomain=${WW_MAIL_DOMAIN}
fi

echo maildomain: ${maildomain}
echo maildir: ${MAILDIR}
echo " "

while [ $1 ] ; do
   crypt=$(/usr/bin/pwgen -AnB 6 1)
   id=$1
   echo "INSERT INTO users (address , crypt , name , maildir , lastread ) VALUES ( '${id}@${maildomain}' , ENCRYPT('$crypt'), '$id' , '$MAILDIR/${maildomain}/${id}/' , NOW() );"  | mysql -h ${WW_ISP_HOST} -u ${WW_ISP_USER} -p${WW_ISP_PASS} ${WW_ISP_DB}
   echo "login: ${id}@${maildomain} password: ${crypt}"
   cat << EOF | /usr/bin/mail -s "Sua Nova conta de e-mail" ${id}@${maildomain}

Esta é a sua nova conta de e-mail, esta
mensagem automática serve apenas para
confirmar a criação da nova conta.

Qualquer dúvida, entre em contato com
o administrador do servidor.
EOF

   shift
done
