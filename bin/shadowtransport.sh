#!/bin/bash

cat /etc/default/wwmail | sed -e 's/: /=/g' > /tmp/wwmail.$$.tmp
source /tmp/wwmail.$$.tmp
rm /tmp/wwmail.$$.tmp

export LOCAL_PART=$1
export DOMAIN=$2

## remove BATV tag from sender
export SENDER=$(echo $3 | sed -e 's/^prvs=[^=]*=\(.*\)$/\1/g' )

ID=filter-$RANDOM
#DEBUG:   set > /tmp/filter.set
#DEBUG:   echo "$ID: sender: $SENDER dest: $LOCAL_PART@$DOMAIN" >> /tmp/filter.log

if [ ${SENDER} == ${LOCAL_PART}@${DOMAIN} ] ; then
   /bin/cat - &> /dev/null
   exit 0
fi

if [ "${SENDER}" == "" ] ; then
   /bin/cat - &> /dev/null
   exit 0
fi

/bin/cat - > /tmp/autoreply.${ID}.orig 2> /dev/null


## if SENDER is on RECIPIENT contacts list, learn as HAM

ONCONTACTS=$(
 echo "SELECT COUNT(contacts.email) FROM roundcube.users,roundcube.contacts
    WHERE users.username='${LOCAL_PART}@${DOMAIN}'
    AND contacts.user_id=users.user_id
    AND contacts.email='${SENDER}'" | mysql -h ${WW_ISP_HOST} -p${WW_ISP_PASS} -r -N -u${WW_ISP_USER} ${WW_ISP_DB} 2> /dev/null)

[ ${ONCONTACTS} -gt 0 ] && ( cat /tmp/autoreply.${ID}.orig | /usr/local/bin/sa-learn-pipe.sh --ham -u ${LOCAL_PART}@${DOMAIN} )

# copia de email para backup

### remetente
SENDER_DOMAIN=`/bin/echo "${SENDER}" | /bin/sed -e 's/[^@]*@\(.*\)/\1/g'`
SENDER_LOCAL_PART=`/bin/echo "${SENDER}" | /bin/sed -e 's/\([^@]*\)@.*/\1/g'`
SENDER_MONITOR=`echo "select address from monitoring where domain='${SENDER_DOMAIN}' or domain='${SENDER_LOCAL_PART}@${SENDER_DOMAIN}';" | mysql -h ${WW_ISP_HOST} -p${WW_ISP_PASS} -r -N -u${WW_ISP_USER} ${WW_ISP_DB} 2> /dev/null`
if [ ${SENDER_MONITOR} ] ; then
   /usr/local/bin/delivercopy.sh ${SENDER_LOCAL_PART} ${SENDER_DOMAIN} ${SENDER_MONITOR} /tmp/autoreply.${ID}.orig
fi

### destinatario
MONITOR=`echo "select address from monitoring where domain='${DOMAIN}' or domain='${LOCAL_PART}@${DOMAIN}';" | mysql -h ${WW_ISP_HOST} -p${WW_ISP_PASS} -r -N -u${WW_ISP_USER} ${WW_ISP_DB} 2> /dev/null`
if [ ${MONITOR} ] ; then
   /usr/local/bin/delivercopy.sh ${LOCAL_PART} ${DOMAIN} ${MONITOR} /tmp/autoreply.${ID}.orig
fi

/bin/rm -rf /tmp/autoreply.${ID}.* &> /dev/null
exit 0
