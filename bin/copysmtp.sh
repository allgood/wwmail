#!/bin/bash

cat /etc/default/wwmail | sed -e 's/: /=/g' > /tmp/wwmail.$$.tmp
source /tmp/wwmail.$$.tmp
rm /tmp/wwmail.$$.tmp

if [ "$1" == "" ] ; then
  cat -
  exit 0
fi

export SENDERADDRESS=$1

DOMAIN=`/bin/echo "$SENDERADDRESS" | /bin/sed -e 's/[^@]*@\(.*\)/\1/g'`
LOCAL_PART=`/bin/echo "$SENDERADDRESS" | /bin/sed -e 's/\([^@]*\)@.*/\1/g'`

#set > /tmp/copysmtp.sudo

# copia de email para backup
MONITOR=`echo "select address from monitoring where domain='$DOMAIN' or domain='$LOCAL_PART@$DOMAIN';" | mysql -h ${WW_ISP_HOST} -p${WW_ISP_PASS} -r -N -u${WW_ISP_USER} ${WW_ISP_DB} 2> /dev/null`
if ! [ $MONITOR ] ; then
   cat -
   exit 0
fi

filename=/tmp/copy.mail.$RANDOM

echo $DOMAIN $LOCAL_PART $MONITOR $filename >> /tmp/copysmtp.log

/bin/cat - | /usr/bin/tee -a $filename

/usr/local/bin/delivercopy.sh $LOCAL_PART $DOMAIN $MONITOR $filename &> /tmp/delivercopy.log

rm -rf $filename &> /dev/null
