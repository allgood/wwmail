#!/bin/bash

cat /etc/default/wwmail | sed -e 's/: /=/g' > /tmp/wwmail.$$.tmp
source /tmp/wwmail.$$.tmp
rm /tmp/wwmail.$$.tmp

echo "UPDATE users SET lastread = now() WHERE address = '${USER}'" | /usr/bin/mysql -h ${WW_ISP_HOST} -p${WW_ISP_PASS} -r -N -u${WW_ISP_USER} ${WW_ISP_DB}
# Finally execute the imap/pop3 binary. If you use both, you'll need two scripts.
exec "$@"
