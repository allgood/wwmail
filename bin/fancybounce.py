#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import fileinput
import re

## MIME support
import email
from email import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

msgdata={}
msgdata["htmlheader"]="""
<html>
  <head><meta charset="UTF-8"></head>
  <body><div style='width: 60%; min-width: 300px; margin: auto; border-width: 5px; border-radius: 15px; border-style: solid; border-color: #019bc6; padding: 5%;'>"""

msgdata["htmlfooter"]="""
   </div>
  </body>
</html>
"""
msgdata["htmlpreon"]="<pre style='overflow: auto; background-color: azure; padding: 1em;'>"
msgdata["htmlpreoff"]="</pre>"

msgdata["htmlh1on"]="<h1>"
msgdata["htmlh1off"]="</h1>"
msgdata["htmlh2on"]="<h2>"
msgdata["htmlh2off"]="</h2>"
msgdata["htmlfailboxon"]="<div style='background-color: #ffe0e0; border-width: 1px; border-style: solid; border-color: #019bc6; padding: 2%; margin: 2%'>"
msgdata["htmlokboxon"]="<div style='background-color: #e0ffe0; border-width: 1px; border-style: solid; border-color: #019bc6; padding: 2%; margin: 2%'>"
msgdata["htmldelayboxon"]="<div style='background-color: #e0e0ff; border-width: 1px; border-style: solid; border-color: #019bc6; padding: 2%; margin: 2%'>"
msgdata["htmlboxoff"]="</div>"

## match status on diagnostic code
statusre = re.compile("^[0-9]{3}[\- ]([0-9]\.[0-9]\.[0-9]) .*")

## mensagens

msgdata["h2.0.0"]="Entrega bem sucedida"
msgdata["d2.0.0"]="A sua mensagem foi aceita pelo servidor do destinatário."

msgdata["h2.1.5"]="Entrega bem sucedida"
msgdata["d2.1.5"]="A sua mensagem foi aceita pelo servidor do destinatário."

msgdata["h4.0.0"]="Entrega falhou"
msgdata["d4.0.0"]="A sua mensagem não pôde ser entregue mas uma tentativa futura pode dar certo."

msgdata["h5.0.0"]="Erro ao enviar a mensagem"
msgdata["d5.0.0"]="Contate o administrador de e-mail para verificar as causas."

msgdata["h5.1.0"]="Endereço rejeitado"
msgdata["d5.1.0"]="Verifique o endereço do destinatário."

msgdata["h5.1.1"]="Usuário desconhecido"
msgdata["d5.1.1"]="Verifique o endereço do destinatário."

msgdata["h5.2.0"]="Problemas na caixa de destino"
msgdata["d5.2.1"]="Ocorreu um problema na caixa de mensagens do destinatário."

msgdata["h5.2.1"]="Caixa de destino desativada"
msgdata["d5.2.1"]="A caixa de mensagens do destinatário está desativada e não está recebendo mensagens no momento."

msgdata["h5.2.2"]="Caixa de destino cheia"
msgdata["d5.2.2"]="A caixa do destinatário está cheia ou está recebendo muitas mensagens no momento."

msgdata["h5.2.3"]="Mensagem muito grande"
msgdata["d5.2.3"]="A mensagem enviada ultrapassa o limite de tamanho aceito pelo servidor de destino."

msgdata["h5.2.4"]="Problema ao interpretar o endereço"
msgdata["d5.2.4"]="Verifique o endereço do destinatário."

msgdata["h5.3.0"]="Sistema de destino rejeitou a mensagem"
msgdata["d5.3.0"]=""

msgdata["h5.4.0"]="Problema de rede ou roteamento"
msgdata["d5.4.0"]=""

msgdata["h5.4.4"]="Impossível encaminhar"
msgdata["d5.4.4"]="Verifique se o endereço foi digitado corretamente."

msgdata["h5.5.0"]="Problema de protocolo"
msgdata["d5.5.0"]="Contate o administrador do sistema de e-mail para verificar as causas."

msgdata["h5.6.0"]="Conteúdo rejeitado"
msgdata["d5.6.0"]="A mensagem pode ser muito grande ou conter anexos que o servidor de destino não aceita. Verifique também seu computador contra vírus."

msgdata["h5.7.0"]="Mensagem rejeitada por razões de seguraça ou política"
msgdata["d5.7.0"]=""

msgdata["h5.7.1"]="Mensagem recusada"
msgdata["d5.7.1"]="A mensagem foi recusada pelo servidor de destino. Contate a administração do sistema de e-mail para verificar as causas."

if (len(sys.argv)>1):
    messageFile = open(sys.argv[1])
else:
    messageFile = sys.stdin

message = email.message_from_file(messageFile)

if message.get_content_type() == "multipart/report":
    parts = message.get_payload()

    if parts[0].get_content_type() == "text/plain" and parts[1].get_content_type() == "message/delivery-status":
        text = parts[0].get_payload()
        fancy = MIMEMultipart('alternative')
        fancy.set_charset('utf-8')
        fancy.attach(MIMEText(text, 'plain', 'utf-8'))

        text = ""
        for item in parts[1].get_payload():

            if item.has_key("Reporting-MTA"):
                text = text + "%(htmlh1on)sRelatório de Envio%(htmlh1off)s"
                text = text + "%(htmlh2on)sFornecido por: " + item["Reporting-MTA"].split()[1] + "%(htmlh2off)s"
                continue;

            if not item.has_key("Action"):
                text = text + "%(htmlfailboxon)s"
                text = text + "<pre>" + item.as_string() + "</pre>"
                text = text + "%(htmlboxoff)s"
                continue

            # read diagnostic code
            if item.has_key("Diagnostic-Code"):
                dc = item["Diagnostic-Code"].split("; ")
                dcmatch = statusre.match(dc[1])
                if dcmatch:
                    extendedstatus = dcmatch.group(1)
                    item.replace_header("Status", extendedstatus)

            if item.has_key("Final-Recipient"):
                address = item["Final-Recipient"].split(";")[1]

            if item.has_key("Original-Recipient"):
                address = item["Original-Recipient"].split(";")[1]

            messageStatus = item["Status"]
            if not msgdata.has_key("h"+messageStatus):
                messageStatus = messageStatus[:4]+"0"

            if not msgdata.has_key("h"+messageStatus):
                messageStatus = messageStatus[:2]+"0.0"

            if item["Action"] == "failed":
                text = text + "%(htmlfailboxon)s"
                text = text + "<h1>%(h" +messageStatus+ ")s</h1>"
                text = text + "<p><b>Destino:</b> "+address+"</p>"
                text = text + "<p>%(d" +messageStatus+ ")s</p>"
                text = text + "%(htmlboxoff)s"
            elif item["Action"] in ["delivered","relayed","expanded"]:
                text = text + "%(htmlokboxon)s"
                text = text + "<h1>%(h" +messageStatus+ ")s</h1>"
                text = text + "<p><b>Destino:</b> "+address+"</p>"
                text = text + "<p>%(d" +messageStatus+ ")s</p>"
                text = text + "%(htmlboxoff)s"
            elif item["Action"] == "delayed":
                text = text + "%(htmldelayboxon)s"
                text = text + "<h1>%(h" +messageStatus+ ")s</h1>"
                text = text + "<p><b>Destino:</b> "+address+"</p>"
                text = text + "<p>%(d" +messageStatus+ ")s</p>"
                text = text + "<p><b>Novas tentativas de entrega serão efetuadas.</b></p>"
                text = text + "%(htmlboxoff)s"
            else:
                text = text + "%(htmlfailboxon)s"
                text = text + "<pre>" + item.as_string() + "</pre>"
                text = text + "%(htmlboxoff)s"


        text = text + "<p>Mais detalhes podem ser obtidos no relatório em anexo. Se precisar de suporte encaminhe essa mensagem completa, incluindo os anexos, para os administradores.</p>"

        fancy.attach(MIMEText(('%(htmlheader)s'+text+'%(htmlfooter)s')%msgdata , 'html' , 'utf-8'))

        parts[0] = fancy

print message.as_string()
