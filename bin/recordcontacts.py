#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

## suportes diversos
from __future__ import with_statement

import sys,os

## leitor de configuração
import ConfigParser
import StringIO

## suporte MIME
import email
from email import Header

## suporte MySQL
import MySQLdb
import MySQLdb.cursors
import MySQLdb.connections

## implementa classe de conexao
class MyConnection(MySQLdb.connections.Connection):

   default_cursor = MySQLdb.cursors.DictCursor

## implementa buscas fetchalldict
class MyCursor(MySQLdb.cursors.DictCursor):

   def row_to_dict ( self , row ):
       keycount={}
       newkey={}
       for i in range(len(row)):
          key = self.description[i][0]
          if keycount.has_key(key):
             keycount["key"]=keycount["key"]+1
             key = "%s_%d"%(key,keycount["key"])
          else:
             keycount["key"]=0;

             newkey[key]=row[i]

       return newkey

   def fetchoneDict (self):
       row = self.fetchone()
       return self.row_to_dict(row)

   def fetchallDict (self):
       result = []
       sqlresult = self.fetchall()
       for row in sqlresult:
          result.append(self.row_to_dict(row))

       return result

def decode_header ( header ):
    decoded = Header.decode_header( header )
    final = u""
    started = 0
    for (part,charset) in decoded:
        if started:
           final = final+" "
        started = 1
        if charset:
           final = final+unicode(part,charset)
        else:
           final = final+unicode(part,"ascii")
    return final




if (len(sys.argv)<3):
  sys.exit(1)

messageID = sys.argv[1]
sender = sys.argv[2]

config = ConfigParser.ConfigParser()
confstring = '[root]\n' + open('/etc/default/wwmail', 'r').read()
config.readfp(StringIO.StringIO(confstring))

##print >> sys.stderr,"%s %s"%(messageID,sender)

ww_roundcube_host = config.get('root','ww_roundcube_host')
ww_roundcube_user = config.get('root','ww_roundcube_user')
ww_roundcube_pass = config.get('root','ww_roundcube_pass')
ww_roundcube_db = config.get('root','ww_roundcube_db')

## inicializa conexao
db_connection = MyConnection( host = ww_roundcube_host , user = ww_roundcube_user , passwd = ww_roundcube_pass , db = ww_roundcube_db , charset="utf8" )
db_cursor = db_connection.cursor()

## encontra ID do remetente no roundcube / cria se nao existir
db_cursor.execute('select user_id from users where username="%s" limit 1;'%sender)
result=db_cursor.fetchall()
if len(result):
  senderID = result[0]["user_id"]
else:
  db_cursor.execute('insert into users ( username , mail_host ,created , language ) values ( "%s" , "localhost" , now() , "pt_BR" );'%sender  )
  senderID = db_connection.insert_id()

## carrega arquivo da mensagem
messageFileName = "/var/spool/exim4/scan/%s/%s.eml"%(messageID,messageID)

with open(messageFileName) as messageFile:
  message = email.message_from_file(messageFile)

#messageTo = decode_header(message["To"])
#messageCc = decode_header(message["Cc"])

recipients = email.utils.getaddresses( message.get_all("to",[]) + message.get_all("cc",[]) )
##print >> sys.stderr,"%s"%recipients

added = 0

## loop para cada destinatario
for i in recipients:
  db_cursor.execute( 'select contact_id from contacts where user_id=%d and email="%s" limit 1'%(senderID,i[1]) )
  result=db_cursor.fetchall()
  if not len(result):
    db_cursor.execute( 'insert into contacts ( user_id , changed , name , email ) values ( %d , now() , "%s" , lower("%s") )'%( senderID, decode_header(i[0]),i[1]) )
    contactID = db_connection.insert_id()
    added = added + 1
  ##else:
    ##print >> sys.stderr,"nao preciso inserir contato %s na lista de %s com o id %d"%(i[1],sender,contactID)

  db_connection.commit()

if added > 0:
  print >> sys.stderr,"true",
else:
  print >> sys.stderr,"false",
