#!/bin/bash

cat /etc/default/wwmail | sed -e 's/: /=/g' > /tmp/wwmail.$$.tmp
source /tmp/wwmail.$$.tmp
rm /tmp/wwmail.$$.tmp

[ -d /etc/wwmail ] || mkdir /etc/wwmail
[ -d /etc/exim4/ssl ] || mkdir /etc/exim4/ssl

cp /etc/letsencrypt/live/${WW_SSL_MAIN}/*.pem /etc/wwmail/
    
cat /etc/letsencrypt/live/${WW_SSL_MAIN}/fullchain.pem \
    /etc/letsencrypt/live/${WW_SSL_MAIN}/privkey.pem > /etc/wwmail/combined.pem

cp /etc/letsencrypt/live/${WW_SSL_MAIN}/fullchain.pem /etc/letsencrypt/live/${WW_SSL_MAIN}/privkey.pem /etc/exim4/ssl/
chmod 640 /etc/exim4/ssl/*
chown root.Debian-exim  /etc/exim4/ssl/*

[ -x /usr/sbin/lighttpd ] && systemctl restart lighttpd
[ -x /usr/sbin/exim4 ] && systemctl restart exim4
[ -x /usr/sbin/dovecot ] && systemctl restart dovecot
