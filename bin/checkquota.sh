#!/bin/bash

source /etc/default/wwmail

if [ "${WW_RELAYTO}" ] ; then
   QUOTACMD="/usr/bin/ssh mail@${WW_RELAYTO}"
else
   QUOTACMD="/usr/bin/sudo -u mail"
fi

RESULT=$(${QUOTACMD} /usr/bin/doveadm quota get -u $1 2> /dev/null | /bin/grep STORAGE | /usr/bin/awk '{ if ( !($4=="-") && $3>=$4){ print "1" ;} else { print "0";}  };')

exit $RESULT
