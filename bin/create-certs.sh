#!/bin/bash

cat /etc/default/wwmail | sed -e 's/: /=/g' > /tmp/wwmail.$$.tmp
source /tmp/wwmail.$$.tmp
rm /tmp/wwmail.$$.tmp

LE_METHOD="--webroot -w /var/www/html"
/usr/bin/lsof -i :80 &> /dev/null || LE_METHOD="--standalone"

certbot certonly  -t \
--renew-by-default --email ${WW_POSTMASTER} --agree-tos ${LE_METHOD} \
 --post-hook "/usr/local/bin/refresh-certs.sh" -d ${WW_SSL_MAIN} \
 $( for DOM in ${WW_SSL_EXTRA} ; do echo "-d ${DOM}" ; done )
