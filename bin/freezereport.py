#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on May/24/2018

@author: allgood
'''

import sys

## leitor de configuração
import ConfigParser
import StringIO

## email
import email
from email import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib

## suporte MySQL
import MySQLdb
import MySQLdb.cursors
import MySQLdb.connections

## utils
import base64
import md5
import urllib
import struct
import time

def decode_header ( header ):
    decoded = Header.decode_header( header )
    final = u""
    started = 0
    for (part,charset) in decoded:
        if started:
           final = final+" "
        started = 1
        if charset:
           final = final+unicode(part,charset)
        else:
           final = final+unicode(part,"ascii")
    return final

def build_timestamp():
    """
    Return a 6 chars url-safe timestamp
    """
    return base64.urlsafe_b64encode(struct.pack("!L",int(time.time())))[:-2]
 
def read_timestamp(t):
    """
    Convert a 6 chars url-safe timestamp back to time
    """
    return struct.unpack("!L",base64.urlsafe_b64decode(t+"=="))[0]


## implementa classe de conexao
class MyConnection(MySQLdb.connections.Connection):

   default_cursor = MySQLdb.cursors.DictCursor

## implementa buscas fetchalldict
class MyCursor(MySQLdb.cursors.DictCursor):

   def row_to_dict ( self , row ):
       keycount={}
       newkey={}
       for i in range(len(row)):
          key = self.description[i][0]
          if keycount.has_key(key):
             keycount["key"]=keycount["key"]+1
             key = "%s_%d"%(key,keycount["key"])
          else:
             keycount["key"]=0;

             newkey[key]=row[i]

       return newkey

   def fetchoneDict (self):
       row = self.fetchone()
       return self.row_to_dict(row)

   def fetchallDict (self):
       result = []
       sqlresult = self.fetchall()
       for row in sqlresult:
          result.append(self.row_to_dict(row))

       return result

sender = sys.argv[1]
messageID = sys.argv[2]
recipients = [x.strip() for x in sys.argv[3].split(',')]

config = ConfigParser.ConfigParser()
confstring = '[root]\n' + open('/etc/default/wwmail', 'r').read()
config.readfp(StringIO.StringIO(confstring))

ww_isp_host = config.get('root','ww_isp_host')
ww_isp_user = config.get('root','ww_isp_user')
ww_isp_pass = config.get('root','ww_isp_pass')
ww_isp_db = config.get('root','ww_isp_db')

## carrega arquivo da mensagem
messageFileName = "/var/spool/exim4/scan/%s/%s.eml"%(messageID,messageID)
with open(messageFileName) as messageFile:
   message = email.message_from_file(messageFile)

msgdata={}
msgdata["sender"] = sender
msgdata["subject"] = decode_header(message["Subject"])
msgdata["date"] = message["Date"]

ts = build_timestamp()

authparams = { 'messageID': messageID ,
               'ts': ts,
               'hash': base64.urlsafe_b64encode(md5.new(messageID+ts+ww_isp_pass).digest())[:-2] }

chpwparams = { 'sender': sender ,
               'ts': ts,
               'hash': base64.urlsafe_b64encode(md5.new(sender+ts+ww_isp_pass).digest())[:-2] }

msgdata["authorizeurl"] = "https://"+config.get('root','ww_ssl_main')+"/wwmail/unfreeze.php?"+urllib.urlencode(authparams)
msgdata["changepwurl"] = "https://"+config.get('root','ww_ssl_main')+"/wwmail/chpwd.php?"+urllib.urlencode(chpwparams)

msgdata["recipients"]=u""
for r in recipients:
   msgdata["recipients"] = msgdata["recipients"] + "\n      " + unicode(r,"utf-8").replace("@",u"\u200B@\u200B")

me = "postmaster@" + config.get('root','ww_mail_domain')


# Create the body of the message (a plain-text and an HTML version).

msgdata["sender"]=unicode(sender,"utf-8").replace("@",u"\u200B@\u200B")

##--guide-rule-60chars-------------------------------------!
message="""%(htmlheader)s\
%(htmlbon)sPrezado(a): %(sender)s%(htmlboff)s
%(htmlpon)s
Houve uma tentativa de envio de mensagem utilizando a sua
senha. Achamos que pode não ser legítima, por favor
verifique os dados da mensagem abaixo:
%(htmlpoff)s
%(htmlpreon)s
%(htmlbon)sData:%(htmlboff)s %(date)s
%(htmlbon)sAssunto:%(htmlboff)s %(subject)s
%(htmlbon)sDestinatários:%(htmlboff)s %(recipients)s
%(htmlpreoff)s
%(htmlpon)s
O envio da mensagem foi suspenso até que você autorize,
para autorizar o envio da mensagem clique no link abaixo:
%(htmlpoff)s
%(htmlauthon)s%(authorizeurl)s%(htmlauthoff)s
%(htmlpon)s
Caso a mensagem não tenha sido enviada por você, por favor
clique no link abaixo para alterar a sua senha:
%(htmlpoff)s
%(htmlchpwon)s%(changepwurl)s%(htmlchpwoff)s
%(htmlpon)s
%(htmlbon)sA mensagem será completamente removida do
servidor após decorridas 24 horas.%(htmlboff)s
%(htmlpoff)s
%(htmlfooter)s
"""

msgdata["htmlheader"]=""
msgdata["htmlbon"]=""
msgdata["htmlboff"]=""
msgdata["htmlpon"]=""
msgdata["htmlpoff"]=""
msgdata["htmlpreon"]=""
msgdata["htmlpreoff"]=""
msgdata["htmlauthon"]=""
msgdata["htmlauthoff"]=""
msgdata["htmlchpwon"]=""
msgdata["htmlchpwoff"]=""
msgdata["htmlfooter"]=""

textplain = unicode(message,"utf-8")%msgdata

msgdata["htmlheader"]="""\
<html>
  <head><meta charset="UTF-8"></head>
  <body><div style='width: 60%; min-width: 300px; margin: auto; border-width: 5px; border-radius: 15px; border-style: solid; border-color: #019bc6; padding: 5%;'>"""

msgdata["htmlfooter"]="""\
   </div>
  </body>
</html>
"""

msgdata["htmlbon"]="<b>"
msgdata["htmlboff"]="</b>"
msgdata["htmlpreon"]="<pre style='overflow: auto; background-color: azure; padding: 1em;'>"
msgdata["htmlpreoff"]="</pre>"
msgdata["htmlpon"]="<p>"
msgdata["htmlpoff"]="</p>"
msgdata["htmlauthon"]="<a style='margin: auto; padding: 10px; border-radius: 5px; display: block; width: 30%; text-decoration: none; text-align: center; color: white; background-color: blue;' href='"
msgdata["htmlauthoff"]="'>Autorizar Envio</a>"
msgdata["htmlchpwon"]="<a style='margin: auto; padding: 10px; border-radius: 5px; display: block; width: 30%; text-decoration: none; text-align: center; color: white; background-color: blue;' href='"
msgdata["htmlchpwoff"]="'>Alterar Senha</a>"

texthtml = unicode(message,"utf-8")%msgdata

# Create message container - the correct MIME type is multipart/alternative.
msg = MIMEMultipart('alternative')
msg.set_charset('utf-8')

msg['Subject'] = u"\U0001F6A7 Envio Interditado \U0001F6A7 Favor confirmar envio"
msg['From'] = u'"Servidor de e-mail" <%s>'%me
msg['To'] = sender

# Record the MIME types of both parts - text/plain and text/html.
part1 = MIMEText(textplain , 'plain' , 'utf-8')
part2 = MIMEText(texthtml, 'html' , 'utf-8')

# Attach parts into message container.
# According to RFC 2046, the last part of a multipart message, in this case
# the HTML message, is best and preferred.
msg.attach(part1)
msg.attach(part2)

s = smtplib.SMTP('localhost')
s.sendmail(me, sender, msg.as_string())
s.quit()
