#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on May/24/2018

@author: allgood
'''

import sys

## leitor de configuração
import ConfigParser
import StringIO

## suporte MySQL
import MySQLdb
import MySQLdb.cursors
import MySQLdb.connections

## implementa classe de conexao
class MyConnection(MySQLdb.connections.Connection):

   default_cursor = MySQLdb.cursors.DictCursor

## implementa buscas fetchalldict
class MyCursor(MySQLdb.cursors.DictCursor):

   def row_to_dict ( self , row ):
       keycount={}
       newkey={}
       for i in range(len(row)):
          key = self.description[i][0]
          if keycount.has_key(key):
             keycount["key"]=keycount["key"]+1
             key = "%s_%d"%(key,keycount["key"])
          else:
             keycount["key"]=0;

             newkey[key]=row[i]

       return newkey

   def fetchoneDict (self):
       row = self.fetchone()
       return self.row_to_dict(row)

   def fetchallDict (self):
       result = []
       sqlresult = self.fetchall()
       for row in sqlresult:
          result.append(self.row_to_dict(row))

       return result


# less than 2 parameters presume its everything ok // TODO: am I wrong?
if (len(sys.argv)<2):
  sys.exit(0)

sender = sys.argv[1]
recipients = [x.strip() for x in sys.argv[2].split(',')]

config = ConfigParser.ConfigParser()
confstring = '[root]\n' + open('/etc/default/wwmail', 'r').read()
config.readfp(StringIO.StringIO(confstring))

##print >> sys.stderr,"%s %s"%(messageID,sender)

ww_roundcube_host = config.get('root','ww_roundcube_host')
ww_roundcube_user = config.get('root','ww_roundcube_user')
ww_roundcube_pass = config.get('root','ww_roundcube_pass')
ww_roundcube_db = config.get('root','ww_roundcube_db')

## inicializa conexao
db_connection = MyConnection( host = ww_roundcube_host , user = ww_roundcube_user , passwd = ww_roundcube_pass , db = ww_roundcube_db , charset="utf8" )
db_cursor = db_connection.cursor()

## não valida se for um volume_sender
db_cursor.execute('select address from isp.volume_senders where address="%s";'%sender)
result=db_cursor.fetchall()
if len(result):
  print "Volume Sender"
  sys.exit(0)

## encontra ID do remetente no roundcube / cria se nao existir
db_cursor.execute('select user_id from users where username="%s" limit 1;'%sender)
result=db_cursor.fetchall()
if len(result):
  senderID = result[0]["user_id"]
else:
  db_cursor.execute('insert into users ( username , mail_host ,created , language ) values ( "%s" , "localhost" , now() , "pt_BR" );'%sender  )
  senderID = db_connection.insert_id()

## inicializa variáveis
firstletter=recipients[0][0]
counter=0

## loop para cada destinatario
for r in recipients:
  ## primeiro caractere diferente, autoriza
  if r[0] != firstletter:
     print "Not alphabetical"
     sys.exit(0)
     
  ## qualquer um dos destinos já na agenda, autoriza
  db_cursor.execute( 'select contact_id from contacts where user_id=%d and email="%s" and del=0 limit 1'%(senderID,r) )
  result=db_cursor.fetchall()
  if not len(result):
     counter=counter+1
     ## se acumular mais que 9, freeze
     if counter > 9:
        print ">9 similar recipients"
        sys.exit(1)
  else:
     ## está na agenda, autoriza
     print "On contacts"
     sys.exit(0)

print "ok"
sys.exit(0)
