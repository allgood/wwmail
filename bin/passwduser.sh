#!/bin/bash

cat /etc/default/wwmail | sed -e 's/: /=/g' > /tmp/wwmail.$$.tmp
source /tmp/wwmail.$$.tmp
rm /tmp/wwmail.$$.tmp

maildomain=

## parametros
## -domain  altera dominio do email

DONE=

while ! [ $DONE ] ; do
   if [ "$1" == "-domain" ] ; then
      maildomain=$2
      shift
      shift
   else
      DONE=1
   fi
done

if ! [ $maildomain ] ; then
  maildomain=${WW_HOST_DOMAIN}
fi

if ! [ ${maildomain} ] ; then
  maildomain=${WW_MAIL_DOMAIN}
fi  
  
echo maildomain: $maildomain
echo " "

while [ $1 ] ; do
   crypt=`/usr/bin/pwgen -AnB 6 1`
   id=$1
   if echo "update users set crypt = encrypt('$crypt') where address='${id}@${maildomain}' limit 1;" | mysql -vv -h ${WW_ISP_HOST} -u ${WW_ISP_USER} -p${WW_ISP_PASS} ${WW_ISP_DB} | grep "1 row affected" &> /dev/null  ; then
      echo "login: ${id}@${maildomain} password: ${crypt}"
   else
      echo "erro ao definir senha para ${id}@${maildomain}"
   fi
   shift
done
