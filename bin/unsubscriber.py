#!/usr/bin/env python

'''
Created on 08/09/2013

@author: allgood
'''

import sys
import socket
import os

## suport MIME
import email
import re
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEMessage import MIMEMessage

import urllib
import subprocess

## leitor de configuração
import ConfigParser
import StringIO

## lê a configuração
config = ConfigParser.ConfigParser()
confstring = '[root]\n' + open('/etc/default/wwmail', 'r').read()
config.readfp(StringIO.StringIO(confstring))

HOSTNAME=socket.gethostbyaddr(socket.gethostname())[0]
DOMAIN=config.get('root','ww_mail_domain')

inmsg = email.message_from_file(sys.stdin)

rp = ""
mail = ""
subject = ""
http = ""
spfaddr = ""
expandedTo = ""
envelopeTo = ""

if inmsg.has_key("Envelope-to"):
    envelopeTo = inmsg["Envelope-to"]

if inmsg.has_key("Return-Path"):
    result = re.search("^<?([^>]*)>?$",inmsg["Return-Path"].strip())
    if result:
        rp = result.group(1)
    if inmsg.has_key("Received-SPF"):
        result = re.search("^pass.*domain of ([^ ]*).*$",inmsg["Received-SPF"].strip())
        if result:
            spfaddr=result.group(1)

if inmsg.has_key("List-Unsubscribe"):
    lu = inmsg["List-Unsubscribe"].split(",")
    for h in lu:
        result = re.search("^<?(https?://[^>]*)>?$",h.strip())
        if result:
            http = result.group(1)

        result = re.search("^<?mailto:([^\?>]*)((\?subject=([^>&]*)[^>]*)|)>?$",h.strip())
        if result:
            mail = result.group(1)
            if result.group(4):
                subject = urllib.unquote(result.group(4))


if rp and rp == spfaddr:
    msg = MIMEMultipart("report")

    msg["From"]="Mailer-Daemon@" + DOMAIN
    msg["Subject"]="Delivery Failed"
    msg["To"]=rp
    if (len(sys.argv)>1):
        expandedTo = sys.argv[1]
        msg["X-Failed-Recipients"]=expandedTo

    msg.attach( MIMEText("SPAM Detected in your message") )

    reportText = ""
    if envelopeTo:
        reportText = reportText + "Original-Recipient: rfc822;%s\n"%(envelopeTo)

    if expandedTo and expandedTo<>envelopeTo:
        reportText = reportText + "Final-Recipient: rfc822;%s\n"%(expandedTo)

    reportText = reportText + "Action: failed\n"
    reportText = reportText + "Status: 5.7.1 (Delivery not authorized, message refused)\n"

    dsnmsg = MIMEText( reportText+"\n" )
    dsnmsg.add_header("Reporting-MTA", "dns")
    dsnmsg.set_param( HOSTNAME , "" , "Reporting-MTA" )
    dsn = MIMEMessage(dsnmsg,"delivery-status")

    dsn.__delitem__("MIME-Version")

    ## remove default headers
    dsnmsg.__delitem__("MIME-Version")
    dsnmsg.__delitem__("Content-Type")
    dsnmsg.__delitem__("Content-Transfer-Encoding")

    msg.attach(dsn)

    inmsg.set_payload("", "us-ascii")

    headers = MIMEMessage(inmsg,"rfc822-headers")
    headers.__delitem__("MIME-Version")
    msg.attach(headers)

    try:
        p = subprocess.Popen(["/usr/sbin/sendmail","-f","<>","-i",rp] , stdin=subprocess.PIPE )
        p.communicate(msg.as_string())
    except:
        None

if http:
    try:
        site = urllib.urlopen(http)
        if site:
            conteudo = site.read()
            site.close()
    except:
            None

sys.exit(0)
