<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class DomainsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function listDomains() {
      if (Gate::allows('list-domains')) {
        $result = DB::table('domains')->select('domain')->get();
        return response()->json($result);
      }
      return response()->json([])->setStatusCode(404);
    }

    public function showOneDomain($domain) {
      if (Gate::allows('support-user',$domain))
      {
        $result = DB::table('domains')->select('domain','storage', 'accounts', 'active')->where('domain',$domain)->get();
        return response()->json($result);
      }
      return response()->json([])->setStatusCode(401);
    }

    public function createDomain(Request $request, $domain) {
      if (Gate::allows('site-admin'))
      {
        try {
          $result = DB::table('domains')->insert([['domain' => $domain]]);
        } catch(\PDOException $e) {
          return response()->json($e)->setStatusCode(409);
        }
        if (!$result) {
          return response()->json([])->setStatusCode(500);
        }
        return response()->json($request->all());
      }
      return response()->json([])->setStatusCode(401);
    }

    public function updateDomain(Request $request, $domain) {
      if (Gate::allows('admin-user',$domain))
      {
        $allowedFields = [ "storage" , "accounts" ];
        $values = $request->all();
        foreach (array_keys($values) as $field) {
          if (!in_array($field , $allowedFields)) {
            return response()->json([])->setStatusCode(401);
          }
        }
        try {
          $result = DB::table('domains')->where('domain',$domain)->update($values);
        } catch(\PDOException $e) {
          return response()->json($e)->setStatusCode(409);
        }
        if (!$result) {
          return response()->json([])->setStatusCode(500);
        }
        return response()->json($request);
      }
      return response()->json([])->setStatusCode(401);
    }

    public function deleteDomain($domain) {
      if (Gate::allows('site-admin'))
      {
        $result = DB::table('domains')->where('domain',$domain)->delete();
        return response()->json($result);
      }
      return response()->json([])->setStatusCode(401);
    }

}
