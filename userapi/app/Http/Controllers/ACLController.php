<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class ACLController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function listAcl($address) {
      if (Gate::allows('support-user',$address))
      {
        $result = DB::table('roles')->select('id','user','target','role')->where('target',$address)->get();
        return response()->json($result);
      }
      return response()->json([])->setStatusCode(401);
    }

    public function getAclDetails($aclid) {
      $result = DB::table('roles')->select('id','user','target','role')->where('id',$aclid)->get();
      if ($result{0} && Gate::allows('support-user',$result{0}->target)) {
        return response()->json($result);
      }

      return response()->json([])->setStatusCode(401);
    }

    public function createAcl(Request $request, $target) {
      if (Gate::allows('admin-user',$request["target"]))
      {
        try {
          $result = DB::table('roles')->insert($request->all());
        } catch(\PDOException $e) {
          return response()->json($e)->setStatusCode(409);
        }
        if (!$result) {
          return response()->json([])->setStatusCode(500);
        }
        return response()->json($request->all());
      }
      return response()->json([])->setStatusCode(401);
    }

    public function updateAcl(Request $request, $aclid) {
      if (Gate::allows('site-admin'))  //FIXME: better permission check
      {
        $allowedFields = [ "role" ];
        $values = $request->all();
        foreach (array_keys($values) as $field) {
          if (!in_array($field , $allowedFields)) {
            return response()->json([])->setStatusCode(401);
          }
        }
        try {
          $result = DB::table('roles')->where('id',$aclid)->update($values);
        } catch(\PDOException $e) {
          return response()->json($e)->setStatusCode(409);
        }
        if (!$result) {
          return response()->json([])->setStatusCode(500);
        }
        return response()->json($request);
      }
      return response()->json([])->setStatusCode(401);
    }

    public function deleteAcl($aclid) {
      if (Gate::allows('site-admin')) //FIXME: better permission check
      {
        $result = DB::table('roles')->where('id',$aclid)->delete();
        return response()->json($result);
      }
      return response()->json([])->setStatusCode(401);
    }

}
