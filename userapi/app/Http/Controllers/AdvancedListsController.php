<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class AdvancedListsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public $advancedLists = [

      "fromwhitelist" => [ "fields" => [ "address" => [ "title" => "Endereço ou Domínio do remetente" ,
                                                        "type" => "emailordomain" ]] ,
                           "title" => "Lista branca de remetentes" ,
                           "description" => "Relaciona endereços de e-mail e domínios de remetentes que não sofrem filtragem pelo AntiSPAM." ,
                           "role" => "site-support" ,
                           "key" => "address" ] ,

      "fromblacklist" => [ "fields" => [ "address" => [ "title" => "Endereço ou Domínio do remetente" ,
                                                        "type" => "emailordomain" ]] ,
                           "title" => "Lista negra de remetentes" ,
                           "description" => "Relaciona endereços de e-mail e domínios de remetentes que são recusados pelo servidor." ,
                           "role" => "site-support" ,
                           "key" => "address" ] ,

      "volume_senders" => [ "fields" => [ "address" => [ "title" => "Endereço do usuário" ,
                                                         "type" => "email" ]] ,
                            "title" => "Remetentes de alto volume" ,
                            "description" => "Relaciona usuários autorizados a enviar grandes volumes de mensagens. Útil para envio por sistemas automáticos, como Nota Fiscal Eletrônica." ,
                            "role" => "site-support" ,
                            "key" => "address" ] ,

      "monitoring" => [ "fields" => [ "domain" => [  "title" => "Endereço ou Domínio a ser monitorado" ,
                                                     "type" => "emailordomain" ] ,
                                      "address" => [  "title" => "Endereço do usuário gerente" ,
                                                      "type" => "email" ]] ,
                        "title" => "Monitoramento de mensagens" ,
                        "description" => "Configura o sistema de monitoramento de mensagens enviadas e recebidas pelos usuários do servidor." ,
                        "role" => "site-admin" ,
                        "key" => "domain" ]

     ];

    public function getLists() {
      if (Gate::allows('site-support')) {
        $lists = [];
        $currentRole = Gate::allows('site-admin') ? "site-admin" : "site-support";
        foreach(array_keys($this->advancedLists) as $list) {
          if ( $currentRole == "site-admin" || $this->advancedLists[$list]["role"] == $currentRole ) {
            $lists[] = [
                         "listName" => $list ,
                         "title" => $this->advancedLists[$list]["title"] ,
                         "description" => $this->advancedLists[$list]["description"]
                        ];
          }
        }
        return response()->json($lists);
      }
      return response()->json([])->setStatusCode(404);
    }

    public function getListStructure($list) {
      if ( array_key_exists($list , $this->advancedLists ) && Gate::allows($this->advancedLists[$list]["role"])) {
        return response()->json($this->advancedLists[$list]);
      }
      return response()->json([])->setStatusCode(404);
    }

    public function listRecords($list) {
      if ( array_key_exists($list , $this->advancedLists ) && Gate::allows($this->advancedLists[$list]["role"])) {
        $result = DB::table($list)->get();
        return response()->json($result);
      }
      return response()->json([])->setStatusCode(404);
    }

    public function getRecord($list, $key) {
      if ( array_key_exists($list , $this->advancedLists ) && Gate::allows($this->advancedLists[$list]["role"]))
      {
        $result = DB::table($list)->where($this->advancedLists[$list]["key"],$key)->get();
        return response()->json($result);
      }
      return response()->json([])->setStatusCode(401);
    }

    public function createRecord(Request $request, $list , $key) {
      if ( array_key_exists($list , $this->advancedLists ) && Gate::allows($this->advancedLists[$list]["role"]))
      {
        $values = $request->all();
        try {
          $result = DB::table($list)->insert([$values]);
        } catch(\PDOException $e) {
          return response()->json($e)->setStatusCode(409);
        }
        if (!$result) {
          return response()->json([])->setStatusCode(500);
        }
        return response()->json($request->all());
      }
      return response()->json([])->setStatusCode(401);
    }

    public function updateRecord(Request $request, $list, $key) {
      if ( array_key_exists($list , $this->advancedLists ) && Gate::allows($this->advancedLists[$list]["role"]))
      {
        $allowedFields = array_keys($this->advancedLists[$list]["fields"]);
        $values = $request->all();
        foreach (array_keys($values) as $field) {
          if (!in_array($field , $allowedFields)) {
            return response()->json([])->setStatusCode(401);
          }
        }
        try {
          $result = DB::table($list)->where($this->advancedLists[$list]["key"],$key)->update($values);
        } catch(\PDOException $e) {
          return response()->json($e)->setStatusCode(409);
        }
        if (!$result) {
          return response()->json([])->setStatusCode(500);
        }
        return response()->json($request);
      }
      return response()->json([])->setStatusCode(401);
    }

    public function deleteRecord($list, $key) {
      if ( array_key_exists($list , $this->advancedLists ) && Gate::allows($this->advancedLists[$list]["role"]))
      {
        $result = DB::table($list)->where($this->advancedLists[$list]["key"],$key)->delete();
        return response()->json($result);
      }
      return response()->json([])->setStatusCode(401);
    }

}
