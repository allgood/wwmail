<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

use OTPHP\TOTP;
use ParagonIE\ConstantTime\Base32;

use PWGen\PWGen;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function showOneUser($address) {
      if (Gate::allows('read-user',$address))
      {
        $result = DB::table('users')->select('address','name', 'quota', 'spamreject','totpactive')->where('address',$address)->get();
        return response()->json($result);
      }
      return response()->json([])->setStatusCode(401);
    }

    public function createUser(Request $request, $address) {
      if (Gate::allows('create-user',$address))
      {
        $pwd = (new PWGen(6, false, true, false, true))->generate();
        $request["crypt"] = crypt($pwd,"$1$".substr(base64_encode(openssl_random_pseudo_bytes(6)),0,8)."$");
        $request["lastread"] = DB::raw('NOW()');
        try {
          $result = DB::table('users')->insert($request->all());
        } catch(\PDOException $e) {
          return response()->json($e)->setStatusCode(409);
        }
        if (!$result) {
          return response()->json([])->setStatusCode(500);
        }
        $welcomeResult = $this->sendWelcomeMessage($address , $request["name"]);
        return response()->json(array("address" => $address, "password" => $pwd , "welcomeMessage" => $welcomeResult ));
      }
      return response()->json([])->setStatusCode(401);
    }

    public function updateUser(Request $request, $address) {
      if (Gate::allows('update-user',$address))
      {
        $allowedFields = [ "name" , "spamreject" ];
        if (Gate::allows('admin-user',$address)) {
          array_push($allowedFields , "quota" , "totpactive" );
        }
        $values = $request->all();
        foreach (array_keys($values) as $field) {
          if (!in_array($field , $allowedFields)) {
            return response()->json([])->setStatusCode(401);
          }
        }
        try {
          $result = DB::table('users')->where('address',$address)->update($values);
        } catch(\PDOException $e) {
          return response()->json($e)->setStatusCode(409);
        }
        if (!$result) {
          return response()->json([])->setStatusCode(500);
        }
        return response()->json($request);
      }
      return response()->json([])->setStatusCode(401);
    }

    public function deleteUser($address) {
      if (Gate::allows('delete-user',$address))
      {
        $result = DB::table('users')->where('address',$address)->delete();
        return response()->json($result);
      }
      return response()->json([])->setStatusCode(401);
    }

    public function listUsers($domain = "") {
      if (Gate::allows('list-users')) {
        $query = DB::table('users')->select('address','name', 'quota', 'spamreject','totpactive')
            ->whereIn('gen_domain', function($query) {
              $query->select('domain')->from('domains');
            });

        if ($domain) {
          $query = $query->where('gen_domain',$domain);
        }

        if (!Gate::allows('site-support')) {
          $query = $query->where(function($query) {
              $query->whereIn('address', function($query) {
                $query->select('target')->from('roles')
               ->where('user', Auth::user()->id);
              })
              ->orWhereIn('gen_domain', function($query) {
                $query->select('target')->from('roles')
                ->where('user', Auth::user()->id);
              });
          });
        }
        $result = $query->get();
        return response()->json($result);
      }
      return response()->json([])->setStatusCode(401);
    }

    public function checkPermission($permission , $parameter="") {
      return response()->json(Gate::allows($permission,$parameter));
    }

    public function generatePassword($address) {
      if (Gate::allows('set-pwd',$address))
      {
        $pwgen = new PWGen(6, false, true, false, true);
        $newPwd = $pwgen->generate();
        $result = DB::table('users')->where('address', $address)
             ->update(['crypt' => crypt($newPwd,"$1$".substr(base64_encode(openssl_random_pseudo_bytes(6)),0,8)."$") ,
                       'lastread' => DB::raw('NOW()') ]);
        if ($result == 0) {
          return response()->json(["result" => $result])->setStatusCode(404);
        }
        return response()->json(array("address" => $address, "password" => $newPwd, "result" => $result ));
      }
      return response()->json([])->setStatusCode(401);
    }

    public function getAccessLevel($address) {
      if (Gate::allows('access-level')) {
        $level = ( Gate::allows('site-admin') || Gate::allows('admin-user',$address) ) ? "admin" :
                 ( ( Gate::allows('site-support') || Gate::allows('support-user',$address) ) ? "support" : "" );
        return response()->json(array("address" => $address , "level" => $level ));
      }
      return response()->json([])->setStatusCode(401);
    }

    public function OTPStart($address, $code=NULL) {
      if (Gate::allows('setup-otp',$address)) {

        if ( $code ) {
          $user = DB::table('users')->select('totpsecret')->where([ ['address',$address] , ['totpactive','>',0] ] )->first();
          if ( ! $user->totpsecret ) {
            return response()->json([]);
          };
          $oldotp = new TOTP($address,$user->totpsecret);
          if (!$oldotp->verify($code)) {
            return response()->json([]);
          }
        }

        if ( !$code && DB::table('users')->select('totpsecret')->where([ ['address',$address] , ['totpactive','>',0] ] )->exists()) {
          return response()->json([]);
        };

        $secret = Base32::encode(random_bytes(20));
        $otp = new TOTP($address,$secret);
        $otp->setIssuer("ww-mail");
        $parameters = $otp->getParameters();

        $result = DB::table('users')->where('address',$address)->update(['totpsecret' => $otp->getSecret() , 'totpactive' => 0]);

        return response()->json([ 'uri' => $otp->getProvisioningUri() , 'parameters' => $parameters ]);
      }
      return response()->json([])->setStatusCode(401);
    }

    public function OTPConfirm($address,$code) {
      if (Gate::allows('setup-otp',$address)) {
        $user = DB::table('users')->select('totpsecret')->where('address',$address)->first();
        if ( ! $user->totpsecret ) {
          return response()->json([]);
        };

        $otp = new TOTP($address,$user->totpsecret);

        $result = $otp->verify($code);

        if ($result) {
          DB::table('users')->where('address',$address)->update(['totpactive' => 1]);
        } else {
          return response()->json([$result])->setStatusCode(401);
        }

        return response()->json([$result]);
      }
      return response()->json([])->setStatusCode(401);
    }

    public function sendWelcomeMessage($address , $name) {
      // Habilita exceptions
      $mail = new PHPMailer(true);
      try {
        $mail->CharSet = 'UTF-8';
        $mail->setFrom(getenv("WW_POSTMASTER"), 'Postmaster');
        $mail->addAddress($address, $name);

        $htmlheader = "<html>
          <head><meta charset=\"UTF-8\"></head>
          <body><div style='width: 60%; min-width: 300px; margin: auto; border-width: 5px; border-radius: 15px; border-style: solid; border-color: #019bc6; padding: 5%;'>";
        $htmlfooter = "</div></body></html>";
        $pon = "<p>";
        $poff = "</p>";

        $msg1 = "Esta é a sua nova conta de e-mail, esta\n".
                "mensagem automática serve apenas para\n".
                "confirmar a criação da nova conta.";
        $msg2 = "Qualquer dúvida, entre em contato com\n".
                "o administrador do servidor.";

        $mail->isHTML(true);
        $mail->Subject = 'Bem vindo à sua nova conta de e-mail';
        $mail->Body    = $htmlheader . $pon . $msg1 . $poff . $pon . $msg2 . $poff . $htmlfooter;
        $mail->AltBody = $msg1 . "\n\n" . $msg2 ;

        $mail->send();
        return ["sent" => true];
      } catch (Exception $e) {
        return ["sent" => false , "error" => $mail->ErrorInfo];
      }
    }

}
