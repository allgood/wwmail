<?php namespace App\Http\Middleware;

/*
   based on original code from:
   https://gist.github.com/carbontwelve/821b110fc725af0f00fc
*/

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class BasicWebAuth
{
    /**
     * Name of the realm
     *
     * @var string
     */
    private $realm = 'Restricted area';

    /**
     * Valid Users
     * @var array
     */
    private $users = array(
        'username' => 'password'
    );

    private function validateUser($username, $password) {
      return DB::table('users')->select('address')
                      ->whereRaw('address = ? and crypt=encrypt(?,crypt)',[$username,$password])
                      ->exists();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      $headers = [
          'Access-Control-Allow-Origin'      => '*',
          'Access-Control-Allow-Methods'     => 'POST, GET, OPTIONS, PUT, DELETE',
          'Access-Control-Allow-Credentials' => 'true',
          'Access-Control-Max-Age'           => '86400',
          'Access-Control-Allow-Headers'     => 'Content-Type, Authorization, X-Requested-With'
      ];

      error_log("handling ".$request->getMethod());

      if ($request->isMethod('OPTIONS'))
      {
          return response()->json('{"method":"OPTIONS"}', 200, $headers);
      }

      if ( false === $request->header('PHP_AUTH_USER', false) )
      {
        return $this->notAuthorisedResponse();
      } else {
        // If password is incorrect
        if ( ! $this->validateUser( $request->header('PHP_AUTH_USER') , $request->header('PHP_AUTH_PW') ) )
        {
            $notAuthorized = $this->notAuthorisedResponse();
            $notAuthorized->withHeaders($headers);
            return $notAuthorized;
        }
      }

      $response = $next($request);

      foreach($headers as $key => $value)
      {
        $response->header($key, $value);
      }

      return $response;
    }

    private function notAuthorisedResponse()
    {
        /** @var Response $response */
        $response = new Response();
        // do not advertise http basic auth to avoid browser popup dialog
        // $response->header('WWW-Authenticate', 'Basic realm="'. $this->realm .'"');
        $response->setStatusCode(401);
        $response->setContent('401: HTTP Basic Auth failed');
        return $response;
    }
}
