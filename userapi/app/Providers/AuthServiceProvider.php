<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Auth\GenericUser;
use Illuminate\Support\Facades\DB;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * check if user can administrate the $target
     */
    private function isAdmin($user,$target) {
      $targetDomain = preg_replace('/^[^@]+@/', '', $target);
      return DB::table('roles')->select('id')
                      ->where('user',$user->getAuthIdentifier())
                      ->whereIn('target',[$target,$targetDomain,""])
                      ->where('role','admin')
                      ->exists();
    }

    /**
     * check if user can support the $target
     */
    private function isSupport($user,$target) {
      $targetDomain = preg_replace('/^[^@]+@/', '', $target);

      return $user->getAuthIdentifier()===$target
             || DB::table('roles')->select('id')
                      ->where('user',$user->getAuthIdentifier())
                      ->whereIn('target',[$target,$targetDomain,""])
                      ->whereIn('role',['admin','support'])
                      ->exists();
    }

    /**
     * check if user is an administrator (of anything)
     */
    private function isAnAdmin($user) {
      return DB::table('roles')->select('id')
                      ->where('user',$user->getAuthIdentifier())
                      ->where('role','admin')
                      ->exists();
    }

    /**
     * check if user is a supporter (of anything)
     */
    private function isASupporter($user) {
      return DB::table('roles')->select('id')
                      ->where('user',$user->getAuthIdentifier())
                      ->whereIn('role',['admin','support'])
                      ->exists();
    }

    /**
     * check if user is a supporter of whole site
     */
    private function isSiteSupporter($user) {
      return DB::table('roles')->select('id')
                      ->where('user',$user->getAuthIdentifier())
                      ->where('target','')
                      ->whereIn('role',['admin','support'])
                      ->exists();
    }

    /**
     * check if user is an admin of whole site
     */
    private function isSiteAdmin($user) {
      return DB::table('roles')->select('id')
                      ->where('user',$user->getAuthIdentifier())
                      ->where('target','')
                      ->where('role','admin')
                      ->exists();
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
          return new GenericUser(['id' => $request->header('PHP_AUTH_USER')]);
        });

        Gate::define('read-user' , function ($user, $address) {
          return ($user->getAuthIdentifier() === $address) || $this->isSupport($user,$address);
        });

        Gate::define('create-user' , function ($user,$address) {
          return $this->isAdmin($user, $address);
        });

        Gate::define('update-user' , function ($user,$address) {
          return $this->isSupport($user, $address);
        });

        Gate::define('delete-user' , function ($user, $address) {
          return $user->getAuthIdentifier()!=$address && $this->isAdmin($user, $address);
        });

        Gate::define('set-pwd' , function ($user, $address) {
          return $this->isSupport($user, $address);
        });

        Gate::define('setup-otp' , function ($user, $address) {
          return ($user->getAuthIdentifier() === $address);
        });

        Gate::define('list-users' , function ($user) {
          return $this->isASupporter($user);
        });

        Gate::define('list-domains' , function ($user) {
          return $this->isASupporter($user);
        });

        Gate::define('site-support' , function ($user) {
          return $this->isSiteSupporter($user);
        });

        Gate::define('site-admin' , function ($user) {
          return $this->isSiteAdmin($user);
        });

        Gate::define('access-level' , function ($user) {
          return !empty($user->getAuthIdentifier());
        });

        Gate::define('support-user' , function ($user, $address) {
          return $this->isSupport($user, $address);
        });

        Gate::define('admin-user' , function ($user, $address) {
          return $this->isAdmin($user, $address);
        });
    }
}
