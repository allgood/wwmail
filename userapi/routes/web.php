<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// keep just to run the unit test
$router->get('/', function () use ($router) {
    return $router->app->version();
});

// REST operations for user
// read user
$router->get('user/{address:[0-9A-Za-z_.@]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'UsersController@showOneUser']);

// create user
$router->post('user/{address:[0-9A-Za-z_.@]+}',
  [ 'middleware' => 'basic.auth' , 'uses' => 'UsersController@createUser']);

// update user
$router->put('user/{address:[0-9A-Za-z_.@]+}',
  [ 'middleware' => 'basic.auth' , 'uses' => 'UsersController@updateUser']);

// delete user
$router->delete('user/{address:[0-9A-Za-z_.@]+}',
  [ 'middleware' => 'basic.auth' , 'uses' => 'UsersController@deleteUser']);

// get access level string
$router->get('accesslevel/{address:[0-9A-Za-z_.@]+}',
  [ 'middleware' => 'basic.auth' , 'uses' => 'UsersController@getAccessLevel']);

// change password
$router->get('setpw/{address:[0-9A-Za-z_.@]+}',
  [ 'middleware' => 'basic.auth' , 'uses' => 'UsersController@generatePassword']);


// REST operations for domain
// read domain
$router->get('domain/{domain:[0-9A-Za-z_.]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'DomainsController@showOneDomain']);

// create domain
$router->post('domain/{domain:[0-9A-Za-z_.]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'DomainsController@createDomain']);

// update domain
$router->put('domain/{domain:[0-9A-Za-z_.]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'DomainsController@updateDomain']);

// delete domain
$router->delete('domain/{domain:[0-9A-Za-z_.]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'DomainsController@deleteDomain']);


// REST operations for acl (roles)
// list acl for one target
$router->get('acl/{address:[0-9A-Za-z_.@]+[A-Za-z]}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'ACLController@listAcl']);

// read one ACL declaration
$router->get('acl/{id:[0-9]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'ACLController@getAclDetails']);

// create ACL
$router->post('acl/{target:[0-9A-Za-z_.@]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'ACLController@createAcl']);

// update ACL
$router->put('acl/{id:[0-9]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'ACLController@updateAcl']);

// delete ACL
$router->delete('acl/{id:[0-9]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'ACLController@deleteAcl']);


$router->get('listusers',
 [ 'middleware' => 'basic.auth' , 'uses' => 'UsersController@listUsers']);

$router->get('listusers/{domain:[0-9A-Za-z_.]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'UsersController@listUsers']);

$router->get('listdomains',
 [ 'middleware' => 'basic.auth' , 'uses' => 'DomainsController@listDomains']);

$router->get('checkpermission/{permission:[^/]+}[/{parameter}]',
 [ 'middleware' => 'basic.auth' , 'uses' => 'UsersController@checkPermission']);

$router->get('otp/start/{address:[0-9A-Za-z_.@]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'UsersController@OTPStart']);

$router->get('otp/start/{address:[0-9A-Za-z_.@]+}/{code:[0-9]{6}}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'UsersController@OTPStart']);

$router->get('otp/confirm/{address:[0-9A-Za-z_.@]+}/{code:[0-9]{6}}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'UsersController@OTPConfirm']);


// Advanced lists
$router->get('advanced/lists',
 [ 'middleware' => 'basic.auth' , 'uses' => 'AdvancedListsController@getLists']);

$router->get('advanced/structure/{list:[a-z_]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'AdvancedListsController@getListStructure']);

$router->get('advanced/{list:[a-z_]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'AdvancedListsController@listRecords']);

// REST operations for advanced lists
// read one list record
$router->get('advanced/{list:[a-z_]+}/{key:[^/]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'AdvancedListsController@getRecord']);

// create record on advanced list
$router->post('advanced/{list:[a-z_]+}/{key:[^/]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'AdvancedListsController@createRecord']);

// update record on advanced list
$router->put('advanced/{list:[a-z_]+}/{key:[^/]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'AdvancedListsController@updateRecord']);

// delete record on advanced list
$router->delete('advanced/{list:[a-z_]+}/{key:[^/]+}',
 [ 'middleware' => 'basic.auth' , 'uses' => 'AdvancedListsController@deleteRecord']);


// OPTIONS -> let the middleware deal with it
$router->options('{blackhole:.*}', [ 'middleware' => 'basic.auth' ]);
