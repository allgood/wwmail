<?php

require_once 'functions.php';
require_once __DIR__.'/../vendor/autoload.php';

$wwconfig = parse_ini_string(file_read_without_comments('/etc/default/wwmail'));

putenv('APP_ENV=local');
putenv('APP_DEBUG=true');
putenv('APP_KEY=');
putenv('APP_TIMEZONE=UTC');

putenv('DB_CONNECTION=mysql');
putenv('DB_HOST='.$wwconfig['WW_ISP_HOST']);
putenv('DB_PORT=3306');
putenv('DB_DATABASE='.$wwconfig['WW_ISP_DB']);
putenv('DB_USERNAME='.$wwconfig['WW_ISP_USER']);
putenv('DB_PASSWORD='.$wwconfig['WW_ISP_PASS']);

putenv('CACHE_DRIVER=file');
putenv('QUEUE_DRIVER=sync');

putenv('WW_MAIL_DOMAIN='.$wwconfig['WW_MAIL_DOMAIN']);
putenv('WW_POSTMASTER='.$wwconfig['WW_POSTMASTER']);

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();

// $app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

// $app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
// ]);

// $app->routeMiddleware([
//     'auth' => App\Http\Middleware\Authenticate::class,
// ]);

$app->routeMiddleware([
   'basic.auth' => App\Http\Middleware\BasicWebAuth::class,
]);


/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

// $app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__.'/../routes/web.php';
});

return $app;
