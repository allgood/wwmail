<?php

function file_read_without_comments($filepath) {
  $string='';
  if (($file = @fopen($filepath,'r')) !== false) {
    while (($line = fgets($file)) !== false) {
      $line=trim($line);
      if (strlen($line)>0 && $line[0] != "#") {
        $string.=$line."\n";
      }
    }
    fclose($file);
  }
  return $string;
}
